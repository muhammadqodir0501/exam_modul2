package model;

import java.util.UUID;

public class Category extends BaseModel{
    private String name;
    private UUID fatherCategoryId;
    public boolean hasCategory = false;


    public UUID getFatherCategoryId() {
        return fatherCategoryId;
    }

    public void setFatherCategoryId(UUID fatherCategoryId) {
        this.fatherCategoryId = fatherCategoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Category{" +
                "name='" + name + '\''
                + "id='" + id + '\'' +
                '}';
    }
}
