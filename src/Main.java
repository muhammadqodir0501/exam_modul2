import model.Category;
import model.Product;
import service.CategoryService;
import service.ProductService;

import java.sql.SQLOutput;
import java.util.Scanner;
import java.util.UUID;

public class Main {

    static Scanner scannerStr = new Scanner(System.in);
    static Scanner scannerInt = new Scanner(System.in);

    public static void main(String[] args) {

        CategoryService categoryService = new CategoryService();
        ProductService productService = new ProductService();
        int stepCode = 10;

        while(stepCode!=0) {
            System.out.println(" 1. Add Category.  2. Show Main Categories.  3.Get category by main category id ." +
                    " 4. Get Products.  5. Add category to category. 6. Add product to category. 7. Delete category.");
            stepCode = scannerInt.nextInt();

            if (stepCode == 1) {
                Category category = addCategoryFromConsole();
                if (categoryService.addCategory(category)) {
                    System.out.println("Category '" + category.getName() + "' added successfully");
                    System.out.println("1. Add a category to '" + category.getName() + "' category");
                    if (!category.hasCategory) {
                        System.out.println("2. Add product to the '" + category.getName() + "' category");
                        int stepCode3 = 0;
                        stepCode3 = scannerInt.nextInt();
                        switch (stepCode3) {
                            case 1 -> {

                                Category childCategory = addCategoryFromConsole();
                                if (categoryService.addCategoryToCategory(category.getId(), childCategory)) {
                                    System.out.println("Category '" + childCategory.getName() + "' added to '"
                                            + category.getName() + "' successfully");
                                }
                            }
                            case 2 -> {

                                Product product = addProductFromConsole();
                                if (productService.addProduct(product, category)) {
                                    System.out.println("Product '" + product.getName() + "' added successfully");
                                } else {
                                    System.out.println("Product '" + product.getName() + "' already added to the '"
                                            + category.getName() + "' category ");
                                }
                            }
                        }

                    }


                } else {
                    System.out.println("Category '" + category.getName() + "' already added ");
                }


            } else if (stepCode == 2) {
                Category[] categoryList = categoryService.getFatherCategories();
                getCategories(categoryList);
            } else if (stepCode == 3) {
                System.out.print("Enter main category id : ");
                String id = scannerStr.nextLine();

                Category[] childCategory = categoryService.getCategoryByFatherId(UUID.fromString(id));
                getCategories(childCategory);
            } else if (stepCode == 4) {
                System.out.println("Enter category id to get products");
                String id = scannerStr.nextLine();
                Product[] productsList = productService.getProducts(UUID.fromString(id));
                getProducts(productsList);

            } else if (stepCode == 5) {

                System.out.println("Enter category id to add a new category");
                String id = scannerStr.nextLine();
                if (categoryService.hasCategory(UUID.fromString(id)) != null) {
                    Category childCategory = addCategoryFromConsole();
                    if(categoryService.addCategoryToCategory(UUID.fromString(id), childCategory)){
                        System.out.println("Category successfully added");
                    }
                } else {
                    System.out.println("There is category with id you entered");
                }
            } else if (stepCode == 6) {
                System.out.println("Enter category id to add a new product");
                String id = scannerStr.nextLine();
                if (categoryService.hasCategory(UUID.fromString(id)) != null) {
                    Product product = addProductFromConsole();
                    Category category = categoryService.hasCategory(UUID.fromString(id));
                    if(productService.addProduct(product, category)){
                        System.out.println("Product successfully added");
                    }
                } else {
                    System.out.println("The product cannot be added to the category you enter or the product is already exist");
                }
            } else if (stepCode == 7) {
                System.out.println("Enter category id to delete ");
                String id = scannerStr.nextLine();
                if(categoryService.deleteCategoriesAndChild(UUID.fromString(id)) && productService.deleteProduct(UUID.fromString(id))){
                    System.out.println("All categories and products with this id were deleted");
                }else if(categoryService.deleteCategoriesAndChild(UUID.fromString(id))){
                    System.out.println("All categories with this id were deleted");
                }else if(productService.deleteProduct(UUID.fromString(id))){
                    System.out.println("All products with this id were deleted");
                }else{
                    System.out.println("Something went wrong");
                }
            }
        }

    }

    public static Category addCategoryFromConsole(){
        Category category = new Category();
        System.out.print("Enter category name : ");
        category.setName(scannerStr.nextLine());

        return category;
    }

    public static void getCategories(Category[] categories) {
        for(Category category : categories){
            if(category != null) {
                System.out.println(category.toString() + "\n");
            }
        }
    }

    public static void getProducts(Product[] products){
        for(Product product : products){
            if(product != null) {
                System.out.println(product.toString() + "\n");
            }
        }
    }

    public static Product addProductFromConsole(){
        Product product = new Product();
        System.out.print("Enter product name : ");
        product.setName(scannerStr.nextLine());
        System.out.print("Enter product price : ");
        product.setPrice(scannerInt.nextInt());
        return product;
    }



}