package service;

import model.Category;
import model.Product;

import java.util.UUID;

public class CategoryService {

     Category[] categories = new Category[10000];
    int categoryIndex = 0;

    public boolean addCategory(Category category) {
        if (hasCategory(category.getId()) == null) {
            categories[categoryIndex++] = category;
            return true;
        }
        return false;
    }


    public Category hasCategory(UUID categoryId) {
        for(int i = 0; i < categoryIndex; i++){
            if(categories[i]!=null && categories[i].getId().equals(categoryId)){
                return categories[i];
            }
        }
        return null;
    }

    public Category hasCategoryByName(Category category){
        for(int i = 0; i < categoryIndex; i++){
            if(categories[i]!=null && categories[i].getName().equals(category.getName())){
                return categories[i];
            }
        }
        return null;
    }

    public Category[] getFatherCategories() {
        int allfathers = 0;
        for (int i = 0; i < categoryIndex; i++) {
            for (int j = i; j < categoryIndex; j++) {
                if (categories[i].getFatherCategoryId() == null) {
                    allfathers++;
                }
            }

        }

        Category[] fathers = new Category[allfathers];
        allfathers = 0;
        for(int i = 0; i < categoryIndex; i++){
            if(categories[i].getFatherCategoryId() == null){
                fathers[allfathers] = categories[i];
                allfathers++;
            }
        }
        return fathers;
    }



    public boolean addCategoryToCategory(UUID fatherCategoryId, Category category) {
        if(hasCategory(fatherCategoryId) != null && hasCategoryByName(category) == null){
            Category theCategory = hasCategory(fatherCategoryId);
            category.setFatherCategoryId(theCategory.getId());
            categories[categoryIndex++] = category;
            Category hasChildCategory = hasCategory(fatherCategoryId);
            hasChildCategory.hasCategory = true;
            return true;
        }
        return false;
    }

    public Category[] getCategoryByFatherId(UUID fatherId){
        int indexCategory = 0;
        for(int i = 0; i < categoryIndex; i++){
            if( categories[i] != null && categories[i].getFatherCategoryId() != null
                    && categories[i].getFatherCategoryId().equals(fatherId)){
                indexCategory++;
            }
        }
        Category[] categories1 = new Category[indexCategory];
        indexCategory = 0;
        for(int i = 0; i < categoryIndex; i++){
            if(categories[i] != null && categories[i].getFatherCategoryId() != null
                    && categories[i].getFatherCategoryId().equals(fatherId)){
                categories1[indexCategory++] = categories[i];
            }
        }
        return categories1;
    }



    public boolean deleteCategoriesAndChild(UUID id){
        for (int i = 0 ; i < categoryIndex; i++){
            if (categories[i] != null && categories[i].getId().equals(id)){
                categories[i]=null;
                for (int j=i; j < categoryIndex-1; j++){
                    categories[j]=categories[j+1];
                }
            }
            categories[categoryIndex-1]=null;
        }

        for (int i=0; i < categoryIndex ; i++){
            if (categories[i] != null && categories[i].getFatherCategoryId().equals(id)){
                deleteCategoriesAndChild(categories[i].getId());
                // categories[i] = null;
            }
        }
        return true;
    }



}
