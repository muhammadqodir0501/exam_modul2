package service;

import model.Category;
import model.Product;

import java.util.UUID;

public class ProductService {
    Product[] products = new Product[1000000];
    int productIndex = 0;

    public boolean addProduct(Product product, Category category) {
        if(hasProduct(product, category) == null && !category.hasCategory){
            product.setCategoryId(category.getId());
            products[productIndex++] = product;
            return true;
        }
        return false;
    }

    private Product hasProduct(Product product, Category category) {
        for(int i = 0; i < productIndex; i++){
            if(products[i].getName().equals(product.getName()) && products[i].getCategoryId().equals(category.getId())){
                return products[i];
            }
        }
        return null;
    }

    public Product[] getProducts(UUID id) {
        int allProducts = 0;
        for(int i = 0; i < productIndex; i++) {
            if(products[i].getCategoryId().equals(id)){
                allProducts++;
            }
        }

        Product[] newProduct = new Product[allProducts];
        allProducts = 0;
        for(int i = 0; i < productIndex; i++){
            if(products[i].getCategoryId().equals(id)){
                newProduct[allProducts] = products[i];
                allProducts++;
            }
        }
        return newProduct;
    }


    public boolean deleteProduct(UUID id) {
        for (int i = 0; i < productIndex; i++) {
            if (products[i] != null && products[i].getId().equals(id)) {
                products[i] = null;
                for (int j = i; j < productIndex - 1; j++) {
                    products[j] = products[j + 1];
                }
            }
            products[productIndex - 1] = null;
        }
        return true;
    }
}
